# Lolin D1 mini + DHT22

This directory contains the code for the NodeMCU Lolin D1 mini board and just a DHT22 temperature / humidity sensor

Use the libraries included in the directory:
- Adafruit DHT Humidity & Temperature Sensor https://travis-ci.org/adafruit/DHT-sensor-library.svg?branch=master
- Adafruit Unified Sensor Driver https://github.com/adafruit/Adafruit_Sensor
- RunningAverage https://github.com/RobTillaart/Arduino/tree/master/libraries/RunningAverage
- base64_arduino Base64 encoder/decoder https://travis-ci.org/Densaugeo/base64_arduino.svg?branch=master

Compile it with Arduino IDE 1.8.9 or later https://www.arduino.cc/en/Main/Software#download

Data will be sent to ThingSpeak's server, so it needs a free or paid account. https://thingspeak.com

**_Warning: This sketch will not work "as is". It needs to be modified adding your personal ThingSpeak's settings._**

## Software environment setup

- [Install Arduino Desktop IDE 1.8.9 or later](https://www.arduino.cc/en/Guide/HomePage) and add this URL: https://arduino.esp8266.com/stable/package_esp8266com_index.json in `File -> Preferences -> Additional Boards Manager URLs` field.
- Open Boards Manager from `Tools > Board` menu and find esp8266 platform. Click install button. Don't forget to select your esp8266 board from `Tools > Board` menu after installation.
- Move `libraries` folder from this repo to Arduino IDE's main libraries folder location (eg. `~/Arduino/libraries` for GNU/Linux)
- WARNING: do not update the DHT library even if asked to do so by Arduino IDE
- Edit the code and customise your settings: Wi-fi ssid, Wi-fi password, ThingSpeak write API key
- Compile and upload the sketch

## Wiring

Use pin D2 for DHT22 data cable
