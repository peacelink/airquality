/*
* Copyright (c) 2019 Piersoft Paolicelli
* MIT License
*/


/**************************
 * INCLUDES               *
 **************************/

#include <DHT.h>
#include "base64.hpp"
#include "RunningAverage.h"
#include <WiFiClientSecure.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiType.h>
#include <ESP8266WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiSTA.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>


/**************************
 * SETTINGS               *
 **************************/

// Settings you MUST customise

const char* ssid = "mywifi"; // your wireless network name (SSID)

const char* pass = "12345678"; // your Wi-Fi network password

String writeAPIKey = "YYYYYYYYY123456"; // your ThingSpeak channel write API key


// Settings you may want to customise

#define pinDHT 4 // pin for DHT22

const int sleepTime = 1; // interval in minutes between readings


// Settings you should not need to customise

const char* server = "184.106.153.149"; // ThingSpeak server IP

const int httpPort = 80; // ThingSpeak server port

#define DHTTYPE DHT22 // type of DHT sensor

#define DEBUG // debug level


/**************************
 * DECLARATIONS           *
 **************************/

// DHT22
DHT dht(pinDHT, DHTTYPE);

// Wi-Fi client
WiFiClient client;

int error;


/**************************
 * FUNCTIONS              *
 **************************/


/**************************
 * Setup                  *
 **************************/
void setup() {
  
  // Set Serial speed (set the same in the monitor)
  Serial.begin(9600);
  Serial.println("");
  
  // connect to Wi-fi
  Serial.print("Connecting to Wi-fi network ");
  Serial.println(ssid);
  
  // set Wi-fi station mode (instead of access point)
  WiFi.mode(WIFI_STA);
  
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    Serial.print(".");
  }
  Serial.println(" connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  // init DHT22
  dht.begin();
  // be patient with DHT 
  delay(1000);
  
  // test we can read some values
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    Serial.println("No data from DHT!");
  } else {
    Serial.println("DHT OK");
  }
  
  // setup finished
  Serial.flush();
  
}


/**************************
 * Loop                   *
 **************************/
void loop() {

  // check Wi-fi connection
  reconnect();
  
  // init DHT22
  dht.begin();
  // be patient with DHT
  delay(1000);

  // Humidity
  float h = dht.readHumidity();
  
  // Temperature
  float t = dht.readTemperature();
  
  // Check values
  if (isnan(h) || isnan(t)) {
    Serial.println("No data from DHT!");
  } else {
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.println("°C");
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.println("%\t");
    Serial.flush();
        
    // Send data to ThingsSpeak
    WiFiClient client;
    if (!client.connect(server, httpPort)) {
      Serial.println("Connection to ThingSpeak failed");
      client.stop();
      return;
    }  
    Serial.println("Connected to ThingSpeak server");
    String url = "GET /update?api_key="+writeAPIKey+"&field1=" + String(t) + "&field2=" + String(h) + " HTTP/1.1\r\n" + "Host: " + server + "\r\n" + "Connection: close\r\n" + "\r\n";
    Serial.print("Requesting URL: ");
    Serial.println(url);
    client.print(url);
    // show server response
    while (client.connected() || client.available()) {
      if (client.available()) {
        String line = client.readStringUntil('\n');
        Serial.println(line);
      }
    }
    client.stop();
  }
  delay(3000);
  
  // Sleep until next run
  Serial.println("Sleep " + String(sleepTime) + " minutes");
  delay(sleepTime * 60 * 1000);
  setup();
}


/*************************************
 * Humidity correction               *
 * by Zbyszek Kiliański & Piotr Paul *
 *************************************/
float normalizePM25(float pm25, float humidity){
  return pm25/(1.0+0.48756*pow((humidity/100.0), 8.60068));
}
float normalizePM10(float pm10, float humidity){
  return pm10/(1.0+0.81559*pow((humidity/100.0), 5.83411));
}


/****************************
 * Connect to Wi-Fi network *
 ****************************/
void reconnect() {
  Serial.print("Connecting to Wi-fi network ");
  Serial.println(ssid);
  
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
  }
  Serial.println(" connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
