/*
* Copyright (c) 2019 Piersoft Paolicelli
* MIT License
*/


/**************************
 * INCLUDES               *
 **************************/

#include <DHT.h>
#include <SDS011.h>
#include "base64.hpp"
#include "RunningAverage.h"
#include <WiFiClientSecure.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WiFiType.h>
#include <ESP8266WiFiAP.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiGeneric.h>
#include <ESP8266WiFiSTA.h>
#include <WiFiUdp.h>
#include <ESP8266mDNS.h>


/**************************
 * SETTINGS               *
 **************************/

// Settings you MUST customise

const char* ssid = "Piersoft"; // your wireless network name (SSID)

const char* pass = "12345678"; // your Wi-Fi network password

String writeAPIKey = "YYYYYYYYY123456"; // your ThingSpeak channel write API key


// Settings you may want to customise

#define pinDHT D7 // pin for DHT22

#define pinSDS_RX D1 // pin for SDS011 TX

#define pinSDS_TX D2 // pin for SDS011 RX

const int sleepTime = 15; // interval in minutes between readings

const int calibrationTime = 15; // calibration time in seconds


// Settings you should not need to customise

const char* server = "184.106.153.149"; // ThingSpeak server IP

const int httpPort = 80; // ThingSpeak server port

#define DHTTYPE DHT22 // type of DHT sensor

#define DEBUG // debug level


/**************************
 * DECLARATIONS           *
 **************************/

// SDS011
SDS011 sds;

// DHT22
DHT dht(pinDHT, DHTTYPE);

// Wi-Fi client
WiFiClient client;

int error;
float p10, p25;
RunningAverage pm25Stats(10);
RunningAverage pm10Stats(10);


/**************************
 * FUNCTIONS              *
 **************************/


/**************************
 * Setup                  *
 **************************/
void setup() {
  
  // Set Serial speed (set the same in the monitor)
  Serial.begin(9600);
  Serial.println("");
  
  // init SDS011
  sds.begin(pinSDS_RX, pinSDS_TX);
  delay(10);
  
  // connect to Wi-fi
  Serial.print("Connecting to Wi-fi network ");
  Serial.println(ssid);
  
  // set Wi-fi station mode (instead of access point)
  WiFi.mode(WIFI_STA);
  
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(250);
    Serial.print(".");
  }
  Serial.println(" connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  // init DHT22
  dht.begin();
  // be patient with DHT 
  delay(1000);
  
  // test we can read some values
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  if (isnan(h) || isnan(t)) {
    Serial.println("No data from DHT!");
  } else {
    Serial.println("DHT OK");
  }
  
  // setup finished
  Serial.flush();
  
}


/**************************
 * Loop                   *
 **************************/
void loop() {
  // reset vars
  pm25Stats.clear();
  pm10Stats.clear();
  
  // check Wi-fi connection
  reconnect();
  
  // wake SDS011 up
  sds.wakeup();
  // be patient with SDS011
  Serial.println("Calibrating SDS011 (" + String(calibrationTime) + " sec)");
  delay(calibrationTime * 1000);
  // read 10 values and make average
  for (int i = 0; i < 10; i++) {
    error = sds.read(&p25, &p10);
    if (!error && p25 < 999 && p10 < 1999) {
      // check isnan
      if(p25==p25 && p10==p10) {
        pm25Stats.addValue(p25);
        pm10Stats.addValue(p10);
        Serial.println("Average PM10: " + String(pm10Stats.getAverage()) + ", PM2.5: "+ String(pm25Stats.getAverage()));
      } else {
        Serial.println("ERROR Nan");
      }
    } else {
      Serial.println("ERROR Invalid value");
    }
    delay(1500);
  }
  
  // init DHT22
  dht.begin();
  // be patient with DHT
  delay(1000);

  // Humidity
  float h = dht.readHumidity();
  
  // Temperature
  float t = dht.readTemperature();
  
  // Check values
  if (isnan(h) || isnan(t)) {
    Serial.println("No data from DHT!");
  } else {
    Serial.print("Temperature: ");
    Serial.print(t);
    Serial.println("°C");
    Serial.print("Humidity: ");
    Serial.print(h);
    Serial.println("%\t");
    Serial.flush();
    
    // Humidity correction for PM 2.5 and PM 10
    float pm25n=normalizePM25(float(pm25Stats.getAverage()),h);
    float pm10n=normalizePM10(float(pm10Stats.getAverage()),h);
    Serial.print("Normalized PM10: ");
    Serial.println(pm10n);
    Serial.print("Normalized PM2.5: ");
    Serial.println(pm25n);
    
    // Send data to ThingsSpeak
    WiFiClient client;
    if (!client.connect(server, httpPort)) {
      Serial.println("Connection to ThingSpeak failed");
      client.stop();
      return;
    }  
    Serial.println("Connected to ThingSpeak server");
    String url = "GET /update?api_key="+writeAPIKey+"&field1=" + String(pm10n) + "&field2=" + String(pm25n) + "&field3=" + String(t) + "&field4=" + String(h) + " HTTP/1.1\r\n" + "Host: " + server + "\r\n" + "Connection: close\r\n" + "\r\n";
    Serial.print("Requesting URL: ");
    Serial.println(url);
    client.print(url);
    // show server response
    while (client.connected() || client.available()) {
      if (client.available()) {
        String line = client.readStringUntil('\n');
        Serial.println(line);
      }
    }
    client.stop();
  }
  delay(3000);
  
  // Sleep until next run
  Serial.println("Sleep " + String(sleepTime) + " minutes");
  sds.sleep();
  delay(sleepTime * 60 * 1000);
  ESP.restart();
}


/*************************************
 * Humidity correction               *
 * by Zbyszek Kiliański & Piotr Paul *
 *************************************/
float normalizePM25(float pm25, float humidity){
  return pm25/(1.0+0.48756*pow((humidity/100.0), 8.60068));
}
float normalizePM10(float pm10, float humidity){
  return pm10/(1.0+0.81559*pow((humidity/100.0), 5.83411));
}


/****************************
 * Connect to Wi-Fi network *
 ****************************/
void reconnect() {
  Serial.print("Connecting to Wi-fi network ");
  Serial.println(ssid);
  
  if (WiFi.status() != WL_CONNECTED) {
    WiFi.begin(ssid, pass);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
  }
  Serial.println(" connected!");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
