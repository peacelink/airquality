# NodeMCU v3 Lolin + Nova PM Sensor SDS011 + DHT22

This directory contains the code for the NodeMCU Lolin board to be used in combnation with:

- DHT22 (temperature and humidity) https://learn.adafruit.com/dht
- SDS011 (PM2.5 and PM10 dust sensor) by Nova Fitness Co.,Ltd http://www.inovafitness.com

Use the libraries included in the directory:
- Adafruit DHT Humidity & Temperature Sensor https://travis-ci.org/adafruit/DHT-sensor-library.svg?branch=master
- Adafruit Unified Sensor Driver https://github.com/adafruit/Adafruit_Sensor
- SDS011 Arduino library By R. Zschiegner reviewed (sds011.cpp) by Piersoft https://gitlab.com/JonixLUG/jonixlug-aqi/SDS011-ricki-z-Piersoft
- RunningAverage https://github.com/RobTillaart/Arduino/tree/master/libraries/RunningAverage
- base64_arduino Base64 encoder/decoder https://travis-ci.org/Densaugeo/base64_arduino.svg?branch=master

Compile it with Arduino IDE 1.8.9 or later https://www.arduino.cc/en/Main/Software#download

Data will be sent to ThingSpeak's server, so it needs a free or paid account. https://thingspeak.com

**_Warning: This sketch will not work "as is". It needs to be modified adding your personal ThingSpeak's settings._**


## Software environment setup

- [Install Arduino Desktop IDE 1.8.9 or later](https://www.arduino.cc/en/Guide/HomePage) and add this URL: https://arduino.esp8266.com/stable/package_esp8266com_index.json in `File -> Preferences -> Additional Boards Manager URLs` field.
- Open Boards Manager from `Tools > Board` menu and find esp8266 platform. Click install button. Don't forget to select your esp8266 board (`NodeMCU 1.0 (ESP-12E Module)`) from `Tools > Board` menu after installation.
- Move `libraries` folder from this repo to Arduino IDE's main libraries folder location (eg. `~/Arduino/libraries` for GNU/Linux)
- WARNING: do not update the DHT library even if asked to do so by Arduino IDE
- Edit the code and customise your settings: Wi-fi ssid, Wi-fi password, ThingSpeak write API key
- Compile and upload the sketch to the NodeMCU board

## Wiring diagram

Based on [Luftdaten](https://luftdaten.info/en/construction-manual/)

![NodeMCU - SDS011 - DHT22](nodeMCU_dht22_wiring.jpg)
